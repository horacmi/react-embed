import { ReduxState, WindowInfo, ContentInfo, WindowPosition, WindowSize, LayoutConfiguration } from '../util/types';
import { DEFAULT_WINDOW_CONTENT, DEFAULT_WINDOW_POSITION, DEFAULT_WINDOW_SIZE } from '../util/constants';
import { WindowAction, WindowActionType } from '../actions/WindowActions';

export function windowsReducer(state: ReduxState = {}, action: WindowAction ): ReduxState {
    switch (action.type) {
        case WindowActionType.CREATE:
            return create(state, action.windowId);
        case WindowActionType.CLOSE:
            return close(state, action.windowId);
        case WindowActionType.RESET:
            return reset(state, action.windowId);
        case WindowActionType.CHANGE_CONTENT:
            return changeContent(state, action.windowId, action.contentInfo);
        case WindowActionType.CHANGE_POSITION:
            return changePosition(state, action.windowId, action.position);
        case WindowActionType.CHANGE_SIZE:
            return changeSize(state, action.windowId, action.size);
        case WindowActionType.LOAD_LAYOUT:
            return loadLayout(state, action.config);
        default:
            return state;
    }
}

function create(state: ReduxState, windowId: number): ReduxState {
    const newWindow: WindowInfo = {
        id: windowId,
        content: DEFAULT_WINDOW_CONTENT,
        position: DEFAULT_WINDOW_POSITION,
        size: DEFAULT_WINDOW_SIZE
    };

    return {
        ...state,
        [windowId]: newWindow
    };
}

function close(state: ReduxState, windowId: number): ReduxState {
    let newState = {...state};
    delete newState[windowId];
    return newState;
}

function reset(state: ReduxState, windowId: number): ReduxState {
    let newState = {...state};
    newState[windowId].content = DEFAULT_WINDOW_CONTENT;
    return newState;
}

function changeContent(state: ReduxState, windowId: number, contentInfo: ContentInfo): ReduxState {
    let newState = {...state};
    newState[windowId].content = contentInfo;
    return newState;
}

function changePosition(state: ReduxState, windowId: number, position: WindowPosition): ReduxState {
    let newState = {...state};
    newState[windowId].position = position;
    return newState;
}

function changeSize(state: ReduxState, windowId: number, size: WindowSize): ReduxState {
    let newState = {...state};
    newState[windowId].size = size;
    return newState;
}

function loadLayout(state: ReduxState, config: LayoutConfiguration): ReduxState {
    const newState: ReduxState = {};
    config.forEach((window: WindowInfo) => newState[window.id] = window);
    return newState;
}