import { ContentInfo, WindowPosition, WindowSize, LayoutConfiguration } from '../util/types';

export enum WindowActionType {
    CREATE = 'CREATE',
    CLOSE = 'CLOSE',
    RESET = 'RESET',
    CHANGE_CONTENT = 'CHANGE_CONTENT',
    CHANGE_POSITION = 'CHANGE_POSITION',
    CHANGE_SIZE = 'CHANGE_SIZE',
    LOAD_LAYOUT = 'LOAD_LAYOUT',
}

export type WindowAction = CreateAction |
                           CloseAction |
                           ResetAction |
                           ChangeContentAction |
                           ChangePositionAction |
                           ChangeSizeAction |
                           LoadLayoutAction;

interface CreateAction {
    type: WindowActionType.CREATE;
    windowId: number;
}

interface CloseAction {
    type: WindowActionType.CLOSE;
    windowId: number;
}

interface ResetAction {
    type: WindowActionType.RESET;
    windowId: number;
}

interface ChangeContentAction {
    type: WindowActionType.CHANGE_CONTENT;
    windowId: number;
    contentInfo: ContentInfo;
}

interface ChangePositionAction {
    type: WindowActionType.CHANGE_POSITION;
    windowId: number;
    position: WindowPosition;
}

interface ChangeSizeAction {
    type: WindowActionType.CHANGE_SIZE;
    windowId: number;
    size: WindowSize;
}

interface LoadLayoutAction {
    type: WindowActionType.LOAD_LAYOUT;
    config: LayoutConfiguration;
}

let nextWindowId = 0;

export function createWindow(): CreateAction {
    return {
        type: WindowActionType.CREATE,
        windowId: nextWindowId++
    };
}

export function closeWindow(windowId: number): CloseAction {
    return {
        type: WindowActionType.CLOSE,
        windowId
    };
}

export function resetWindow(windowId: number): ResetAction {
    return {
        type: WindowActionType.RESET,
        windowId
    };
}

export function changeContentType(windowId: number, contentInfo: ContentInfo): ChangeContentAction {
    return {
        type: WindowActionType.CHANGE_CONTENT,
        windowId,
        contentInfo
    };
}

export function changePosition(windowId: number, position: WindowPosition): ChangePositionAction {
    return {
        type: WindowActionType.CHANGE_POSITION,
        windowId,
        position
    };
}

export function changeSize(windowId: number, size: WindowSize): ChangeSizeAction {
    return {
        type: WindowActionType.CHANGE_SIZE,
        windowId,
        size
    };
}

export function loadLayout(config: LayoutConfiguration): LoadLayoutAction {
    return {
        type: WindowActionType.LOAD_LAYOUT,
        config
    };
}