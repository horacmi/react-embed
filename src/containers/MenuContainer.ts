import { connect } from 'react-redux';
import * as _ from 'lodash';
import AppMenu from '../components/AppMenu';
import { ReduxState, WindowDispatch, LayoutConfiguration } from '../util/types';
import { closeWindow, createWindow, loadLayout } from '../actions/WindowActions';

function mapStateToProps(state: ReduxState) {
    return {
        openedWindows: _.map(state, (window) => window)
    };
}

function mapDispatchToProps(dispatch: WindowDispatch) {
    return {
        onCreateWindow: () => dispatch(createWindow()),
        onCloseWindow: (windowId: number) => dispatch(closeWindow(windowId)),
        onLayoutLoad: (config: LayoutConfiguration) => dispatch(loadLayout(config))
    };
}

const MenuContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppMenu);
export default MenuContainer;