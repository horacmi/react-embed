import { connect } from 'react-redux';
import * as _ from 'lodash';
import AppCanvas from '../components/AppCanvas';
import { ReduxState, WindowDispatch, ContentInfo, WindowPosition, WindowSize } from '../util/types';
import { closeWindow, resetWindow, changeContentType, changePosition, changeSize } from '../actions/WindowActions';

function mapStateToProps(state: ReduxState) {
    return {
        openedWindows: _.map(state, (window) => window)
    };
}

function mapDispatchToProps(dispatch: WindowDispatch) {
    return {
        onCloseWindow: (windowId: number) => dispatch(closeWindow(windowId)),
        onResetWindow: (windowId: number) => dispatch(resetWindow(windowId)),
        onWindowContentChange: (windowId: number, contentInfo: ContentInfo) => 
            dispatch(changeContentType(windowId, contentInfo)),
        onWindowPositionChange: (windowId: number, position: WindowPosition) =>
            dispatch(changePosition(windowId, position)),
        onWindowSizeChange: (windowId: number, size: WindowSize) =>
            dispatch(changeSize(windowId, size)),
    };
}

const CanvasContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppCanvas);
export default CanvasContainer;