import { WindowAction } from '../actions/WindowActions';

export interface ReduxState {
    [windowId: number]: WindowInfo;
}

export type WindowDispatch = (action: WindowAction) => void;

export interface WindowInfo  {
    id: number;
    content: ContentInfo;
    position: WindowPosition;
    size: WindowSize;
}

export interface ContentInfo {
    name: string;
    type: ContentType;
}

export enum ContentType {
    FORM = 'form',
    CHAT = 'chat',
    STREAM = 'video',
    VOD = 'vod',
    CLIP = 'clip'
}

export interface WindowSize {
    width: number;
    height: number;
}

export interface WindowPosition {
    x: number;
    y: number;
}

export type LayoutConfiguration = Array<WindowInfo>;