import { WindowPosition, WindowSize, ContentInfo, ContentType } from './types';

export const DEFAULT_WINDOW_POSITION: WindowPosition = {
    x: 55,
    y: 5
};

export const DEFAULT_WINDOW_SIZE: WindowSize = {
    width: 300,
    height: 300
};

export const MIN_WINDOW_SIZE: WindowSize = {
    width: 280,
    height: 150
};

export const DEFAULT_WINDOW_CONTENT: ContentInfo = {
    name: 'New Window',
    type: ContentType.FORM
};

export const WINDOW_CONTROL_HEIGHT = 16;
export const APP_MENU_WIDTH = 50;