import * as React from 'react';
import LayoutSaver from './LayoutSaver';
import LayoutLoader from './LayoutLoader';
import { LayoutConfiguration } from '../util/types';

interface LayoutManagerProps {
    openedWindows: LayoutConfiguration;
    onLayoutLoad: (config: LayoutConfiguration) => void;
}

interface LayoutManagerState {
    savedLayouts: Array<string>;
}

export default class LayoutManager extends React.Component<LayoutManagerProps, LayoutManagerState> {
    constructor(props: LayoutManagerProps) {
        super(props);
        this.state = {
            savedLayouts: this.getSavedLayouts()
        };

        this.onLayoutDelete = this.onLayoutDelete.bind(this);
        this.onLayoutLoad = this.onLayoutLoad.bind(this);
        this.onLayoutSave = this.onLayoutSave.bind(this);
    }
    
    render() {
        return (
            <div className="layout-manager">
                <LayoutSaver onLayoutSave={this.onLayoutSave} />
                <LayoutLoader 
                    savedLayouts={this.state.savedLayouts}
                    onLoad={this.onLayoutLoad} 
                    onDelete={this.onLayoutDelete} 
                    getLayoutConfig={this.getLayoutConfig}
                />
            </div>
        );
    }

    private getSavedLayouts() {
        return Object.keys(localStorage);
    }

    private onLayoutSave(layoutName: string) {
        const configString = JSON.stringify(this.props.openedWindows);
        localStorage.setItem(layoutName, configString);
        this.setState({ savedLayouts: this.getSavedLayouts() });
    }

    private onLayoutLoad(layoutName: string) {
        const config = this.getLayoutConfig(layoutName);
        this.props.onLayoutLoad(config);
    }

    private onLayoutDelete(layoutName: string) {
        localStorage.removeItem(layoutName);
        this.setState({ savedLayouts: this.getSavedLayouts() });
    }

    private getLayoutConfig(layoutName: string): LayoutConfiguration {
        const configString = localStorage.getItem(layoutName);
        if (configString === null) {
            console.warn('Invalid layout configuration for layout: ' + layoutName);
            return [];
        }

        let config: LayoutConfiguration;
        try {
            config = JSON.parse(configString);
        } catch (SyntaxError) {
            console.warn('Could not parse configuration for layout: ' + layoutName);
            return [];
        }

        return config;
    }
}