import * as React from 'react';
import LayoutPreview from './LayoutPreview';
import { LayoutConfiguration } from '../util/types';

interface LayoutLoaderProps {
    savedLayouts: Array<string>;
    onDelete: (layoutName: string) => void;
    onLoad: (layoutName: string) => void;
    getLayoutConfig: (layoutName: string) => LayoutConfiguration;
}

export default class LayoutLoader extends React.Component<LayoutLoaderProps, {}> {
    render() {
        return (
            <div className="layout-loader">
                <div className="manager-header">
                    List of saved layouts
                </div>

                <div className="layout-list">
                    <ul className="layout-items">
                        {this.props.savedLayouts.map((name) => this.renderLayoutItem(name))}
                    </ul>
                </div>
            </div>
        );
    }

    private renderLayoutItem(name: string): JSX.Element {
        return (
            <li key={name} className="layout-item">
                <LayoutPreview layout={this.props.getLayoutConfig(name)} />
                <div className="layout-item-info">
                    <span className="layout-item-text"> {name} </span>
                    <div className="layout-item-control">
                        <button 
                            className="manager-button layout-item-load" 
                            onClick={() => this.props.onLoad(name)}
                        > 
                            Load 
                        </button>
                        <button 
                            className="manager-button layout-item-delete" 
                            onClick={() => this.props.onDelete(name)}
                        > 
                            Delete
                        </button>
                    </div>
                </div>
            </li>
        );
    }
}