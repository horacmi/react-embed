import * as React from 'react';

import EmbedWindow from './EmbedWindow';
import { WindowInfo, ContentInfo, WindowSize, WindowPosition, LayoutConfiguration } from '../util/types';

export interface AppCanvasProps {
    openedWindows: LayoutConfiguration;
    onCloseWindow: (windowId: number) => void;
    onResetWindow: (windowId: number) => void;
    onWindowContentChange: (windowId: number, contentInfo: ContentInfo) => void;
    onWindowPositionChange: (windowId: number, position: WindowPosition) => void;
    onWindowSizeChange: (windowId: number, size: WindowSize) => void;
}

export default class AppCanvas extends React.Component<AppCanvasProps, {}> {
    render() {
        return (
            <div className="app-canvas">
                {this.props.openedWindows.map((window) => this.renderWindow(window))}
            </div>
        );
    }

    private renderWindow(window: WindowInfo): JSX.Element {
        return (
            <EmbedWindow
                key={window.id}
                windowInfo={window}
                onCloseWindow={this.props.onCloseWindow}
                onResetWindow={this.props.onResetWindow}
                onContentChange={this.props.onWindowContentChange}
                onPositionChange={this.props.onWindowPositionChange}
                onSizeChange={this.props.onWindowSizeChange}
            />
        );
    }
}