import * as React from 'react';
import { WindowInfo } from '../util/types';
import WindowItem from './WindowItem';

interface WindowListProps {
    openedWindows: Array<WindowInfo>;
    onCloseWindow: (windowId: number) => void;
}

export default class WindowList extends React.Component<WindowListProps, {}> {
    render() {
        return (
            <div className="window-list">
                <div className="manager-header"> List of opened windows </div>
                <ul className="window-list-items">
                    {this.props.openedWindows.map((window) => this.renderWindowItem(window))}
                </ul>
            </div>
        );
    }

    private renderWindowItem(windowInfo: WindowInfo) {
        return (
            <WindowItem key={windowInfo.id} windowInfo={windowInfo} onCloseWindow={this.props.onCloseWindow} />
        );
    }
}