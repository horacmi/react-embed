import * as React from 'react';
import WindowList from './WindowList';
import { LayoutConfiguration } from '../util/types';

interface WindowManagerProps {
    openedWindows: LayoutConfiguration;
    onCloseWindow: (windowId: number) => void;
}

export default class WindowManager extends React.Component<WindowManagerProps, {}> {   
    render() {
        return (
            <div className="window-manager">
                <WindowList 
                    openedWindows={this.props.openedWindows}
                    onCloseWindow={this.props.onCloseWindow}
                />
            </div>
        );
    }
}