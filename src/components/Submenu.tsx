import * as React from 'react';

interface SubmenuProps {
    title: string;
}

export default class Submenu extends React.Component<SubmenuProps, {}> {
    render() {
        return (
            <div className="app-submenu">
                <div className="submenu-title">
                    {this.props.title}
                </div>

                <div className="submenu-content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}