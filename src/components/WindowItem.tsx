import * as React from 'react';

import { WindowInfo } from '../util/types';

interface WindowItemProps {
    windowInfo: WindowInfo;
    onCloseWindow: (windowId: number) => void;
}

export default class WindowItem extends React.Component<WindowItemProps, {}> {
    constructor(props: WindowItemProps) {
        super(props);
        this.closeWindow = this.closeWindow.bind(this);
    }

    render() {
        return (
            <li className="window-list-item">
                <span className="window-item-text"> {this.props.windowInfo.content.name} </span>
                <button className="manager-button window-item-close" onClick={this.closeWindow}>
                    Close
                </button>
            </li>
        );
    }

    private closeWindow() {
        this.props.onCloseWindow(this.props.windowInfo.id);
    }
}