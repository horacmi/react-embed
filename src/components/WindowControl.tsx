import * as React from 'react';
import { WindowInfo } from '../util/types';

interface WindowControlProps {
    windowInfo: WindowInfo;
    onDragWindow: (event: React.MouseEvent<HTMLDivElement>) => void;
    isLocked: boolean;
    onLockWindow: () => void;
    onCloseWindow: () => void;
    onResetWindow: () => void;
}

export default class WindowControl extends React.Component<WindowControlProps, {}> {
    constructor(props: WindowControlProps) {
        super(props);
        
        this.onDragStart = this.onDragStart.bind(this);
        this.onLock = this.onLock.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onReset = this.onReset.bind(this);
    }

    render() {
        let lockButtonIcon;
        let titleBarClassList = ['window-titlebar'];

        if (this.props.isLocked) {
            lockButtonIcon = 'fa fa-lock';
        } else {
            lockButtonIcon = 'fa fa-unlock';
            titleBarClassList.push('window-unlocked');
        }

        return (
            <div className="window-control">
                <div className={titleBarClassList.join(' ')} onMouseDown={this.onDragStart}>
                    {this.renderTitlebarText()}
                </div>
                <button
                    className="window-button window-reset"
                    onClick={this.onReset}
                >
                    <i className="fa fa-refresh" />
                </button>
                <button 
                    className="window-button window-toggle" 
                    onClick={this.onLock}
                > 
                    <i className={lockButtonIcon} />
                </button>
                <button 
                    className="window-button window-close" 
                    onClick={this.onClose}
                >
                    <i className="fa fa-times" />
                </button>
            </div>
        );
    }

    private renderTitlebarText(): string {
        return this.props.windowInfo.content.name;
    }

    private onDragStart(event: React.MouseEvent<HTMLDivElement>) {
        this.props.onDragWindow(event);
    }

    private onLock(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.onLockWindow();
    }

    private onClose(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.onCloseWindow();
    }

    private onReset(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.onResetWindow();
    }
}