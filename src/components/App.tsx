import * as React from 'react';

import MenuContainer from '../containers/MenuContainer';
import CanvasContainer from '../containers/CanvasContainer';

export default class App extends React.Component<{}, {}> {
    render() {
        return (
        <div className="react-embed-app">
            <MenuContainer />
            <CanvasContainer />
        </div>      
        );
    }
}