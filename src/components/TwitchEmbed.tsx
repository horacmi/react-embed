import * as React from 'react';
import { ContentInfo, ContentType } from '../util/types';

interface TwitchEmbedProps {
    contentInfo: ContentInfo;
}

export default class TwitchEmbed extends React.Component<TwitchEmbedProps, {}> {
    render() {
        switch (this.props.contentInfo.type) {
            case ContentType.STREAM:
                return this.renderVideo();
            case ContentType.CHAT:
                return this.renderChat();
            default:
                return this.renderVideo();
        }
    }

    private renderVideo(): JSX.Element {
        const srcUrl = `http://player.twitch.tv/?channel=${this.props.contentInfo.name}`;
        return this.renderIFrame(srcUrl);
    }

    private renderChat(): JSX.Element {
        const srcUrl = `http://www.twitch.tv/embed/${this.props.contentInfo.name}/chat`;
        return this.renderIFrame(srcUrl);
    }

    private renderIFrame(srcUrl: string): JSX.Element {
        return (
            <iframe scrolling="no" src={srcUrl} />
        );
    }
}