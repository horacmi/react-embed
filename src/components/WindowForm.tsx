import * as React from 'react';

import { ContentType } from '../util/types';
import { SyntheticEvent } from 'react';

interface WindowFormProps {
    onContentChange: (formData: FormData) => void;
}

interface WindowFormState {
    contentType: ContentType;
}

export interface FormData {
    name: string;
    type: ContentType;
}

export default class WindowForm extends React.Component<WindowFormProps, WindowFormState> {
    constructor(props: WindowFormProps) {
        super(props);
        this.state = {
            contentType: ContentType.STREAM
        };

        this.onSelectChange = this.onSelectChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        const placeholderText = this.getPlaceholderText();

        return (
            <div className="window-form">
            <form className="window-form-direct" onSubmit={this.onSubmit}>
                <input
                    className="window-form-input"
                    type="text" 
                    name="formInput" 
                    id="window-form-input"
                    placeholder={placeholderText}
                />
                <select 
                    name="formSelect" 
                    id="window-form-select"
                    className="window-form-select"
                    onChange={this.onSelectChange}
                >
                    <option value={ContentType.STREAM}> Stream </option>
                    <option value={ContentType.CHAT}> Chat </option>
                    <option value={ContentType.VOD}> VOD </option>
                    <option value={ContentType.CLIP}> Clip </option>
                </select>
                <button className="window-form-submit">Go!</button>
            </form>

            </div>
        );
    }

    private onSelectChange(event: SyntheticEvent<HTMLSelectElement>) {
        const optionValue: ContentType = event.currentTarget.value as ContentType;
        this.setState({contentType: optionValue});
    }

    private onSubmit(event: SyntheticEvent<HTMLFormElement>) {
        event.preventDefault();
        const channelName = event.currentTarget.formInput.value;
        const contentType = event.currentTarget.formSelect.value;

        if (!this.isValidInput(channelName, contentType)) {
            return;
        }

        const formData: FormData = {
            name: channelName,
            type: contentType
        };

        this.props.onContentChange(formData);
    }

    private isValidInput(channelName: string, contentType: ContentType) {
        if (channelName === undefined || channelName.length === 0) {
            return false;
        }

        if (contentType === undefined || contentType === ContentType.FORM) {
            return false;
        }

        return true;
    }

    private getPlaceholderText(): string {
        switch (this.state.contentType) {
            case ContentType.STREAM:
            case ContentType.CHAT:
                return 'Channel name';
            case ContentType.CLIP:
                return 'Clip ID';
            case ContentType.VOD:
                return 'VOD ID';
            default:
                return '';
        }
    }
}