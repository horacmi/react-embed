import * as React from 'react';

import MenuOption from './MenuOption';
import LayoutManager from './LayoutManager';
import Submenu from './Submenu';
import WindowManager from './WindowManager';
import { LayoutConfiguration } from '../util/types';

interface AppMenuProps {
    openedWindows: LayoutConfiguration;
    onCreateWindow: () => void;
    onCloseWindow: (windowId: number) => void;
    onLayoutLoad: () => void;
}

interface AppMenuState {
    activeManager: ActiveManager;
}

enum ActiveManager {
    NONE,
    WINDOW,
    LAYOUT
}

export default class AppMenu extends React.Component<AppMenuProps, AppMenuState> {
    constructor(props: AppMenuProps) {
        super(props);
        this.state = { activeManager: ActiveManager.NONE };
    }

    render() {
        return (
            <div className="app-menu">
                <div className="app-menu-options">
                <MenuOption 
                    onClick={() => this.props.onCreateWindow()} 
                    icon="fa fa-plus"
                    tooltip="Create new window"
                    isSelected={false}
                />

                <MenuOption
                    onClick={() => this.toggleWindowManager()}
                    icon="fa fa-window-maximize"
                    tooltip="Manage opened windows"
                    isSelected={this.state.activeManager === ActiveManager.WINDOW}
                />

                <MenuOption
                    onClick={() => this.toggleLayoutManager()}
                    icon="fa fa-window-restore"
                    tooltip="Manage window layouts"
                    isSelected={this.state.activeManager === ActiveManager.LAYOUT}
                />
                </div>
                {this.renderActiveManager()}
            </div>
        );
    }

    private renderActiveManager(): JSX.Element | null {
        switch (this.state.activeManager) {
            case ActiveManager.WINDOW:
                return this.renderWindowManager();
            case ActiveManager.LAYOUT:
                return this.renderLayoutManager();
            default:
                return null;
        }
    }

    private renderWindowManager(): JSX.Element {
        return (
            <Submenu title="Window Manager">
                <WindowManager 
                    openedWindows={this.props.openedWindows}
                    onCloseWindow={this.props.onCloseWindow}
                />            
            </Submenu>
        );
    }

    private renderLayoutManager(): JSX.Element {
        return (
            <Submenu title="Layout Manager">
                <LayoutManager 
                    openedWindows={this.props.openedWindows}
                    onLayoutLoad={this.props.onLayoutLoad}
                />
            </Submenu>
        );
    }    

    private toggleWindowManager() {
        const manager = (this.state.activeManager === ActiveManager.WINDOW 
                            ? ActiveManager.NONE 
                            : ActiveManager.WINDOW);
        this.setState({activeManager: manager});
    }

    private toggleLayoutManager() {
        const manager = (this.state.activeManager === ActiveManager.LAYOUT 
                            ? ActiveManager.NONE 
                            : ActiveManager.LAYOUT);
        this.setState({activeManager: manager});
    }
}