import * as React from 'react';

interface MenuOptionProps {
    onClick: () => void;
    icon: string;
    tooltip: string;
    isSelected: boolean;
}

export default class MenuOption extends React.Component<MenuOptionProps, {}> {
    render() {
        const classList = ['menu-button'];
        if (this.props.isSelected) {
            classList.push('option-selected');
        }

        return (
            <div className="window-option">
                <button 
                    className={classList.join(' ')}
                    onClick={this.props.onClick}
                >
                <i className={this.props.icon} />
                </button>

                <div className="option-tooltip">
                    {this.props.tooltip}
                </div>
            </div>
        );
    }
}