import * as React from 'react';

import { ContentType, ContentInfo } from '../util/types';
import TwitchEmbed from './TwitchEmbed';
import WindowForm from './WindowForm';

interface WindowContentProps {
    contentInfo: ContentInfo;
    onContentChange: (contentInfo: ContentInfo) => void;
}

export default class WindowContent extends React.Component<WindowContentProps, {}> {

    constructor(props: WindowContentProps) {
        super(props);
    }

    render() {
        return (
            <div className="window-content">
                {this.renderContent()}
            </div>
        );
    }

    private renderContent(): JSX.Element {
        switch (this.props.contentInfo.type) {
            case ContentType.STREAM:
            case ContentType.CHAT:
                return this.renderTwitchEmbed();
            default:
                return this.renderForm();
        }
    }

    private renderForm(): JSX.Element {
        return <WindowForm onContentChange={this.props.onContentChange} />;
    }

    private renderTwitchEmbed(): JSX.Element {
        return (
            <TwitchEmbed contentInfo={this.props.contentInfo} />
        );
    }
}