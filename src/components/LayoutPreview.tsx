import * as React from 'react';
import { WindowSize, LayoutConfiguration, WindowInfo } from '../util/types';
import { APP_MENU_WIDTH } from '../util/constants';

interface LayoutPreviewProps {
    layout: LayoutConfiguration;
}

export default class LayoutPreview extends React.Component<LayoutPreviewProps, {}> {
    canvas: HTMLCanvasElement | null;
    readonly DEFAULT_SIZE = 120;

    componentDidMount() {
        const canvas = this.canvas;
        if (canvas === null) {
            return;
        }

        const windowSize: WindowSize = {
            width: window.innerWidth,
            height: window.innerHeight
        };

        let sizeRatio;
        if (windowSize.width > windowSize.height) {
            sizeRatio = this.DEFAULT_SIZE / (windowSize.width - APP_MENU_WIDTH);
            canvas.width = this.DEFAULT_SIZE;
            canvas.height = windowSize.height * sizeRatio;
        } else {
            sizeRatio = this.DEFAULT_SIZE / windowSize.height;
            canvas.height = this.DEFAULT_SIZE;
            canvas.width = windowSize.width * sizeRatio;
        }

        const ctx = canvas.getContext('2d');
        if (ctx === null) {
            return;
        }

        ctx.fillStyle = '#f1f1f1';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        this.previewLayout(ctx, this.props.layout, sizeRatio);
    }

    render() {
        return (
            <div className="layout-item-preview">
                <canvas 
                    className="layout-canvas" 
                    ref={(canvas) => this.canvas = canvas}
                />
            </div>
        );
    }

    private previewLayout(ctx: CanvasRenderingContext2D, layout: LayoutConfiguration, ratio: number) {
        ctx.beginPath();
        layout.forEach((window) => this.previewWindow(ctx, window, ratio));
        ctx.fillStyle = '#7777ff43';
        ctx.strokeStyle = 'red';
        ctx.fill();
        ctx.stroke();
    }

    private previewWindow(ctx: CanvasRenderingContext2D, window: WindowInfo, ratio: number) {
        ctx.rect((window.position.x - APP_MENU_WIDTH) * ratio, window.position.y * ratio,
                 window.size.width * ratio, window.size.height * ratio);
    }
}