import * as React from 'react';

import WindowContent from './WindowContent';
import WindowControl from './WindowControl';
import { WindowInfo, ContentInfo, WindowPosition, WindowSize } from '../util/types';
import { APP_MENU_WIDTH, MIN_WINDOW_SIZE } from '../util/constants';

interface EmbedWindowProps {
    windowInfo: WindowInfo;
    onCloseWindow: (windowId: number) => void;
    onResetWindow: (windowId: number) => void;
    onContentChange: (windowId: number, contentInfo: ContentInfo) => void;
    onPositionChange: (windowId: number, position: WindowPosition) => void;
    onSizeChange: (windowId: number, size: WindowSize) => void;
}

interface EmbedWindowState {
    isLocked: boolean;
    position: WindowPosition;
    size: WindowSize;
    resizeInfo?: ResizeInfo;
    dragStart?: WindowPosition;
}

interface ResizeInfo {
    startPosition: WindowPosition;
    startSize: WindowSize;
}

export default class EmbedWindow extends React.Component<EmbedWindowProps, EmbedWindowState> {
    constructor(props: EmbedWindowProps) {
        super(props);
        this.state = {
            isLocked: false,
            position: this.props.windowInfo.position,
            size: this.props.windowInfo.size
        };

        this.onDragStart = this.onDragStart.bind(this);
        this.onDrag = this.onDrag.bind(this);
        this.onDragStop = this.onDragStop.bind(this);
        this.onResizeStart = this.onResizeStart.bind(this);
        this.onResizeDrag = this.onResizeDrag.bind(this);
        this.onResizeStop = this.onResizeStop.bind(this);
        this.onLock = this.onLock.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onReset = this.onReset.bind(this);
        this.onContentChange = this.onContentChange.bind(this);
    }

    render() {
        const renderStyle = {
            top: this.state.position.y + 'px',
            left: this.state.position.x + 'px',
            width: this.state.size.width + 'px',
            height: this.state.size.height + 'px'
        };         

        return (
            <div className="embed-window" style={renderStyle}>
                <WindowControl
                    windowInfo={this.props.windowInfo}
                    isLocked={this.state.isLocked}
                    onLockWindow={this.onLock} 
                    onCloseWindow={this.onClose} 
                    onResetWindow={this.onReset}
                    onDragWindow={this.onDragStart}
                />
                <WindowContent
                    contentInfo={this.props.windowInfo.content} 
                    onContentChange={this.onContentChange}
                />

                {this.state.isLocked ? null : this.renderResizeHandle()}
            </div>
        );
    }

    private renderResizeHandle(): JSX.Element {
        return (
            <div className="resize-handle" onMouseDown={this.onResizeStart}>
                <i className="fa fa-th" />
            </div>
        );
    }

    private onLock() {
        this.setState((prevState) => ({
            isLocked: !prevState.isLocked
        }));
    }

    private onClose() {
        this.props.onCloseWindow(this.props.windowInfo.id);
    }

    private onReset() {
        this.props.onResetWindow(this.props.windowInfo.id);
    }

    private onContentChange(content: ContentInfo) {
        this.props.onContentChange(this.props.windowInfo.id, content);
    }

    private onDragStart(event: React.MouseEvent<HTMLDivElement>) {
        if (this.state.isLocked) {
            return;
        }

        document.documentElement.addEventListener('mousemove', this.onDrag, false);
        document.documentElement.addEventListener('mouseup', this.onDragStop, false);

        const dragStart: WindowPosition = {
            x: event.clientX,
            y: event.clientY
        };

        this.setState({
            dragStart
        });
    }

    private onDrag(event: MouseEvent) {
        if (this.state.dragStart === undefined) {
            return;
        }

        let position: WindowPosition;
        if (this.props.windowInfo.position === undefined) {
            position = {
                x: event.clientX - this.state.dragStart.x,
                y: event.clientY - this.state.dragStart.y
            };
        } else {
            position = {
                x: this.props.windowInfo.position.x + event.clientX - this.state.dragStart.x,
                y: this.props.windowInfo.position.y + event.clientY - this.state.dragStart.y
            };
        }

        this.checkPositionBounds(position);

        this.setState({
            position
        });
    }

    private checkPositionBounds(position: WindowPosition) {
        if (position.x < APP_MENU_WIDTH) {
            position.x = APP_MENU_WIDTH;
        }

        if (position.x + this.state.size.width > window.innerWidth) {
            position.x = window.innerWidth - this.state.size.width;
        }

        if (position.y < 0) {
            position.y = 0;
        }

        if (position.y + this.state.size.height > window.innerHeight) {
            position.y = window.innerHeight - this.state.size.height;
        }
    }
    
    private onDragStop(event: MouseEvent) {
        document.documentElement.removeEventListener('mousemove', this.onDrag, false);
        document.documentElement.removeEventListener('mouseup', this.onDragStop, false);
        this.props.onPositionChange(this.props.windowInfo.id, this.state.position);
    }

    private onResizeStart(event: React.MouseEvent<HTMLDivElement>) {
        if (this.state.isLocked) {
            return;
        }

        document.documentElement.addEventListener('mousemove', this.onResizeDrag, false);
        document.documentElement.addEventListener('mouseup', this.onResizeStop, false);

        const resizeInfo: ResizeInfo = {
            startPosition: {
                x: event.clientX,
                y: event.clientY
            },
            startSize: {
                width: this.props.windowInfo.size.width,
                height: this.props.windowInfo.size.height,
            }
        };

        this.setState({
            resizeInfo
        });
    }

    private onResizeDrag(event: MouseEvent) {
        if (this.state.resizeInfo === undefined) {
            return;
        }

        const size: WindowSize = {
            width: (this.state.resizeInfo.startSize.width + 
                    event.clientX - this.state.resizeInfo.startPosition.x),
            height: (this.state.resizeInfo.startSize.height + 
                     event.clientY - this.state.resizeInfo.startPosition.y)
        };

        this.checkSizeBounds(size);

        this.setState({
            size
        });
    }

    private checkSizeBounds(size: WindowSize) {
        if (size.width < MIN_WINDOW_SIZE.width) {
            size.width = MIN_WINDOW_SIZE.width;
        }

        if (size.height < MIN_WINDOW_SIZE.height) {
            size.height = MIN_WINDOW_SIZE.height;
        }
        if (size.width + this.state.position.x > window.innerWidth) {
            size.width = window.innerWidth - this.state.position.x;
        }

        if (size.height + this.state.position.y > window.innerHeight) {
            size.height = window.innerHeight - this.state.position.y;
        }        
    }
    
    private onResizeStop(event: MouseEvent) {
        document.documentElement.removeEventListener('mousemove', this.onResizeDrag, false);
        document.documentElement.removeEventListener('mouseup', this.onResizeStop, false);
        this.props.onSizeChange(this.props.windowInfo.id, this.state.size);
    }
}