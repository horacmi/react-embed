import * as React from 'react';

interface LayoutSaverProps {
    onLayoutSave: (layoutName: string) => void;
}

interface LayoutSaverState {
    errorMessage: string;
}

export default class LayoutSaver extends React.Component<LayoutSaverProps, LayoutSaverState> {
    constructor(props: LayoutSaverProps) {
        super(props);
        this.state = { errorMessage: '' };
        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        return (
            <div className="layout-saver">
                <div className="manager-header">
                    Save current layout
                </div>
                <form className="layout-saver-form" onSubmit={this.onSubmit}>
                    <input 
                        className="layout-saver-input"
                        type="text" 
                        name="layoutName" 
                        id="layoutName" 
                        placeholder="Layout name"
                    />
                    <button className="manager-button layout-saver-button">Save</button>
                </form>
                <div className="layout-saver-error">
                    {this.state.errorMessage}
                </div>
            </div>
        );
    }

    private onSubmit(event: React.SyntheticEvent<HTMLFormElement>) {
        event.preventDefault();
        const layoutName = event.currentTarget.layoutName.value.trim();
        if (layoutName.length === 0) {
            this.setState({ errorMessage: 'Layout name must contain at least 1 character' });
            return;
        }

        const layoutConfig = localStorage.getItem(layoutName);
        if (layoutConfig !== null) {
            this.setState({ errorMessage: 'Layout with given name already exists' });
            return;
        }

        if (this.state.errorMessage.length !== 0) {
            this.setState({ errorMessage: '' });
        }
        return this.props.onLayoutSave(layoutName);
    }
}