import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';
import { windowsReducer } from './reducers/WindowReducers';

import './styles/style.css';
import 'font-awesome/css/font-awesome.min.css';

let store = createStore(windowsReducer);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,        
    document.getElementById('react-streams-root') as HTMLElement
);
registerServiceWorker();
