# React-Streams App

React application that allows embedding content from twitch.tv and youtube.com. Embedded windows have drag-and-drop and resize functionality. Layouts of embedded windows can be saved and loaded using web browsers local storage.

## Getting development version

1. clone repository
2. run `npm install` to install required packages
3. run `npm start` to start development server and launch application